<?php

function vardump($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

class Car
{
    public function __construct($color, $model, $brand, $gear)
    {
        $this->color = $color;
        $this->model = $model;
        $this->brand = $brand;
        $this->gear = $gear;
    }
    public function changeColor($color)
    {
        $this->color = $color;
    }
}

$audi = new Car('white', 'Q7', 'Audi', 'automatic');

$bmw = new Car('black', 'X6', 'BMW', 'automatic');
#$bmw->changeColor('red');

vardump($audi);
vardump($bmw);

class TVSet
{
    public function __construct($model, $brand, $size, $color)
    {
        $this->model = $model;
        $this->brand = $brand;
        $this->size = $size;
        $this->color = $color;
    }
    public function get_price($price)
    {
        $this->price = $price;
    }
}

$samsung = new TVSet('QLED QE55Q7CAM', 'Samsung', '50', 'black');

$lg = new TVSet('OLED55B7V', 'LG', '55', 'grey');
$lg->get_price(108990);

vardump($samsung);
vardump($lg);

class Pen
{
    public function __construct($brand, $color, $type, $material)
    {
        $this->brand = $brand;
        $this->color = $color;
        $this->type = $type;
        $this->material = $material;
    }
    public  function del($parameter)  {
        if (isset($this->$parameter)) {
            unset($this->$parameter);
        }
    }
}

$stabilo = new Pen('STABILO', 'red', 'ball', 'plastic');

$parker = new Pen('PARKER', 'gold', 'ink', 'gold');
$parker->del('brand');

vardump($stabilo);
vardump($parker);

class Duck
{
    public static $counter = 0;
    public function __construct($color, $weight, $breed)
    {
        $this->color = $color;
        $this->weight = $weight;
        $this->breed = $breed;
        self::$counter++;
    }
}

$mulard = new Duck('white', 7, 'mulard');

$star_53 = new Duck('white', '3', 'star_53');

vardump($mulard);
vardump($star_53);
echo Duck::$counter;

class Goods
{
    public function __construct($model, $brand, $price, $discount)
    {
        $this->model = $model;
        $this->brand = $brand;
        $this->price = $price;
        $this->discount = $discount;
    }
    public function get_price($price, $discount)
    {
        $discount_price = $price - ($price * ($discount / 100));
        $this->discount_price = $discount_price;
    }
}

$apple = new Goods('Iphone 10', 'Apple', 60000, 15);
$apple->get_price(60000, 15);

$samsung = new Goods('Galaxy S7', 'Samsung', 30000, 5);

vardump($apple);
vardump($samsung);